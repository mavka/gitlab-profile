# [Mavka](https://mavka.gitlab.io/home)

![Mavka](img/mavka-128.png "Mavka")

A collection of tools to parse, store and communicate [MAVLink](https://mavlink.io/en/) data written in
[Rust](https://www.rust-lang.org/).

- [`Mavio`](https://gitlab.com/mavka/libs/mavinspect) — a minimalistic library for transport-agnostic
  [MAVLink](https://mavlink.io/en/) communication.
- [`MAVSpec`](https://gitlab.com/mavka/libs/mavspec) — a code-generator for [MAVLink](https://mavlink.io/en/).
- [`MAVInspect`](https://gitlab.com/mavka/libs/mavinspect) — a library for parsing [MAVLink](https://mavlink.io/en/) XML definitions.
- [`Maviola`](https://gitlab.com/mavka/libs/maviola) — a high-level MAVLink communication library based on
  `Mavio` that provides a high-level interface for MAVLink messaging and takes care about **stateful** features of the
  protocol: sequencing, message time-stamping, automatic heartbeats, simplifies message signing, and so on.

Check the [`website`](https://mavka.gitlab.io/home) for details.

License
-------

> Here we simply comply with the suggested dual licensing according to
> [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/about.html) (C-PERMISSIVE).

All projects of Mavka family are licensed under either of

* Apache License, Version 2.0
  ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license
  ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

Contribution
------------

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
